const path = require('path');
const IgnoreEmitPlugin = require('ignore-emit-webpack-plugin');

const javascriptConfig = {
  mode: 'production',
  entry: {
    'quick-library-view': './src-assets/scripts/quick-library-view.es6.js',
  },
  output: {
    filename: 'scripts/[name].js',
    path: path.resolve(__dirname, 'assets'),
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'],
            plugins: ['@babel/plugin-proposal-object-rest-spread']
          }
        }
      },
    ]
  }
};

/**
 * Is it possible to use webpack as a sass compiler? Yes! But buggy.
 */
const sassConfig = {
  mode: 'production',
  entry: {
    'admin-field': './src-assets/styles/admin-field.scss'
  },
  output: {
    filename: 'styles/test.js',
    path: path.resolve(__dirname, 'assets'),
  },
  plugins: [
    new IgnoreEmitPlugin(['test.js']),
  ],
  module: {
    rules: [
      {
        test: /^[^_].*\.scss$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: 'styles/[name].css',
            }
          },
          'extract-loader',
          'css-loader?-url',
          'sass-loader'
        ]
      }
    ]
  }
};

module.exports = [
  javascriptConfig, sassConfig
];
