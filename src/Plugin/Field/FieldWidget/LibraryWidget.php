<?php

declare(strict_types=1);

namespace Drupal\library_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\library_field\LibraryService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'library_field_attachment' widget.
 *
 * @FieldWidget(
 *   id = "library_field_attachment_default",
 *   module = "library_field",
 *   label = @Translation("Library Attachment Picker"),
 *   field_types = {
 *     "library_field_attachment"
 *   }
 * )
 */
class LibraryWidget extends WidgetBase {

  /**
   * The permission to use the library field.
   */
  const USE_LIBRARY_PERMISSION = 'use library_field';

  /**
   * The library service.
   *
   * @var \Drupal\library_field\LibraryService
   */
  protected $libraryService;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Constructs a WidgetBase object.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\library_field\LibraryService $libraryService
   *   The library service.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    array $third_party_settings,
    LibraryService $libraryService,
    AccountInterface $currentUser
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->libraryService = $libraryService;
    $this->currentUser = $currentUser;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('library_field.library'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(
    FieldItemListInterface $items,
    array &$form,
    FormStateInterface $form_state,
    $get_delta = NULL
  ) {
    $widgetForm = parent::form($items, $form, $form_state, $get_delta);
    $widgetForm['libraries'] = [
      '#type' => 'hidden',
      '#attached' => [
        'library' => [
          'library_field/field_edit_library',
        ],
      ],
    ];
    $widgetForm['modal'] = [
      '#type' => 'theme',
      '#theme' => 'library_field_admin_quick_list',
    ];
    $widgetForm['#access'] = $this->doAccessLibraryField();
    return $widgetForm;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(
    FieldItemListInterface $items,
    $delta,
    array $element,
    array &$form,
    FormStateInterface $form_state
  ) {
    $value = isset($items[$delta]->value) ? $items[$delta]->value : '';
    $status = isset($items[$delta]->status) ? $items[$delta]->status : TRUE;
    $element = [
      '#title' => $this->t('Library name to attach.'),
      '#title_display' => 'before',
      '#description' => $this->t('A Drupal library found in a libraries file, follows format `module/library`'),
      '#type' => 'textfield',
      '#weight' => 0,
      '#attributes' => [
        'class' => ["data-quick-list-$delta"],
      ],
      '#default_value' => $value,
      '#element_validate' => [
          [$this, 'validate'],
      ],
    ] + $element;
    $statusElement = [
      '#title' => $this->t('Enable this library'),
      '#description' => $this->t('Load the library when the field is displayed'),
      '#weight' => 1,
      '#type' => 'checkbox',
      '#default_value' => $status,
    ];
    $help = [
      '#type' => 'container',
      '#attributes' => [
        '#class' => ['library_field__help-links'],
      ],
      'quick_view' => [
        '#type' => 'markup',
        '#markup' => $this->getQuickViewMarkup($delta),
        '#allowed_tags' => ['a'],
      ],
      'overview_link' => [
        '#type' => 'link',
        '#title' => $this->t('View Libraries Overview Page'),
        '#url' => new Url('library_field.library_overview'),
        '#attributes' => [
          'target' => '_blank',
          'class' => ['button'],
          'aria-label' => $this->t('This link will open a new page in another window that lists all libraries'),
        ],
      ],
      '#weight' => 100,
    ];
    return ['value' => $element, 'help' => $help, 'status' => $statusElement];
  }

  /**
   * Gets the quick view modal link.
   *
   * @param string|int $delta
   *   The delta key of the field.
   *
   * @return string
   *   The link.
   */
  protected function getQuickViewMarkup($delta): string {
    $text = $this->t('Library Quick View');
    $ariaText = $this->t('This will open a modal of libraries on the site');
    return "<a aria-label='$ariaText' href='#' data-micromodal-trigger='quick-list-modal' data-quick-list-delta='$delta' class='library_field__quick-view button'>$text</a>";
  }

  /**
   * Validates a library field.
   *
   * @param array $element
   *   The element to validate.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function validate(array $element, FormStateInterface $form_state): void {
    $value = $element['#value'];
    if (strlen($value) === 0) {
      $form_state->setValueForElement($element, '');
      return;
    }
    if (!$this->libraryService->isValidLibrary($value)) {
      $form_state->setError(
        $element,
        $this->t('The library entered is not a valid library or does not exist')
      );
    }
  }

  /**
   * Checks if the user has permissions to use this field.
   *
   * @return bool
   *   If the user can access the field.
   */
  protected function doAccessLibraryField(): bool {
    return $this->currentUser->hasPermission('bypass node access')
      || $this->currentUser->hasPermission(static::USE_LIBRARY_PERMISSION);
  }

}
