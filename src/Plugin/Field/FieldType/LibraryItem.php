<?php

declare(strict_types=1);

namespace Drupal\library_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'library_field_attachment' field type.
 *
 * @FieldType(
 *   id = "library_field_attachment",
 *   label = @Translation("Library Attacher"),
 *   module = "library_field",
 *   description = @Translation("Adds a field that allows a user to add libraries"),
 *   default_widget = "library_field_attachment_default",
 *   default_formatter = "library_field_attachment_default"
 * )
 */
class LibraryItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'type' => 'text',
          'size' => 'normal',
          'not null' => FALSE,
        ],
        'status' => [
          'type' => 'int',
          'size' => 'tiny',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Library Name'))
      ->setDescription(new TranslatableMarkup('Full name of the library to attach'));

    $properties['status'] = DataDefinition::create('boolean')
      ->setLabel(new TranslatableMarkup('Enabled'))
      ->setDescription(new TranslatableMarkup('If this library should be attached when the field is displayed'));

    return $properties;
  }

}
