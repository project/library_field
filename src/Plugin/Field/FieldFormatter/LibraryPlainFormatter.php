<?php

declare(strict_types=1);

namespace Drupal\library_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'library_field_attachment_admin' formatter.
 *
 * @FieldFormatter(
 *   id = "library_field_attachment_admin",
 *   module = "library_field",
 *   label = @Translation("Admin view (plaintext)"),
 *   field_types = {
 *     "library_field_attachment"
 *   }
 * )
 */
class LibraryPlainFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      if ($item->status) {
        $elements[$delta] = [
          '#type' => 'inline_template',
          '#template' => '{{ value|nl2br }}',
          '#context' => ['value' => $item->value],
        ];
      }
    }

    return $elements;
  }

}
