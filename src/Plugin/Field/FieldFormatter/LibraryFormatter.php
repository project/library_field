<?php

declare(strict_types=1);

namespace Drupal\library_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\library_field\LibraryCacheTagUtil;
use Drupal\library_field\LibraryService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'library_field_attachment' formatter.
 *
 * @FieldFormatter(
 *   id = "library_field_attachment_default",
 *   module = "library_field",
 *   label = @Translation("Attach libraries"),
 *   field_types = {
 *     "library_field_attachment"
 *   }
 * )
 */
class LibraryFormatter extends FormatterBase {

  /**
   * The library service.
   *
   * @var \Drupal\library_field\LibraryService
   */
  protected $libraryService;

  /**
   * LibraryFormatter constructor.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\library_field\LibraryService $libraryService
   *   The library service.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    LibraryService $libraryService
  ) {
    parent::__construct(
      $plugin_id,
      $plugin_definition,
      $field_definition,
      $settings,
      $label,
      $view_mode,
      $third_party_settings
    );
    $this->libraryService = $libraryService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('library_field.library')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      if ($item->status && $this->libraryService->isValidLibrary($item->value)) {
        $elements[$delta] = [
          '#type' => 'hidden',
          '#attached' => [
            'library' => [
              $item->value,
            ],
          ],
        ];
        // For now we only attach this tag for library field entities,.
        if ($this->libraryService->isLibraryEntity($item->value)) {
          $elements[$delta]['#cache'] = [
            'tags' => [LibraryCacheTagUtil::getTagFromLibrary($item->value)],
          ];
        }
      }
    }

    return $elements;
  }

}
