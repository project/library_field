<?php

declare(strict_types=1);

namespace Drupal\library_field;

/**
 * Helper utility for library cache tags.
 */
class LibraryCacheTagUtil {

  /**
   * Generates a custom cache tag based on the library name.
   */
  public static function getTagFromLibrary(string $library): string {
    return 'library_field:' . str_replace('/', ':', $library);
  }

}
