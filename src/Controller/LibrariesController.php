<?php

declare(strict_types=1);

namespace Drupal\library_field\Controller;

use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Controller\ControllerBase;
use Drupal\library_field\LibraryService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controller for viewing libraries.
 */
class LibrariesController extends ControllerBase {

  /**
   * The library service.
   *
   * @var \Drupal\library_field\LibraryService
   */
  protected $libraryService;

  /**
   * LibrariesController constructor.
   *
   * @param \Drupal\library_field\LibraryService $libraryService
   *   The library service.
   */
  public function __construct(LibraryService $libraryService) {
    $this->libraryService = $libraryService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('library_field.library')
    );
  }

  /**
   * Builds the library overview page.
   *
   * @return array
   *   The render array of libraries.
   */
  public function libraryOverViewPage(): array {
    $render = [
      'overview' => [
        '#type' => 'container',
        '#attributes' => [
          'class' => [
            'library-overview-page',
          ],
        ],
        '#cache' => [
          'tags' => [
            'library_info',
          ],
        ],
      ],
    ];
    foreach ($this->libraryService::getLibraryTypes() as $type) {
      if (!empty($this->libraryService->getLibraries()[$type])) {
        $render['overview'][$type] = [
          '#theme' => 'library_field_library_overview',
          '#library_data' => $this->libraryService->getLibraries()[$type],
          '#library_label' => $this->libraryService->getLibraryTypeReadable($type),
          '#library_class' => str_replace('_', '-', $type),
        ];
      }
    }
    return $render;
  }

  /**
   * Endpoint for getting all libraries as JSON response.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The libraries JSON response.
   */
  public function listLibraries(): JsonResponse {
    $cache = [
      '#cache' => [
        'tags' => [
          'library_info',
        ],
      ],
    ];
    $response = new CacheableJsonResponse($this->libraryService->getLibrarySimpleList());
    $response->addCacheableDependency(CacheableMetadata::createFromRenderArray($cache));
    return $response;
  }

}
