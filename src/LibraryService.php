<?php

declare(strict_types=1);

namespace Drupal\library_field;

use Drupal\Core\Asset\LibraryDiscoveryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * The library service helps find and validate libraries.
 */
class LibraryService {

  use StringTranslationTrait;

  /**
   * The library entity module name.
   */
  const LIBRARY_ENTITY_NAME = 'library_field_entity';

  /**
   * The core module type.
   */
  const LIBRARY_TYPE_CORE_MODULE = 'core_module';

  /**
   * The core theme type.
   */
  const LIBRARY_TYPE_CORE_THEME = 'core_theme';

  /**
   * A custom or contrib module.
   */
  const LIBRARY_TYPE_MODULE = 'module';

  /**
   * A custom or contrib theme.
   */
  const LIBRARY_TYPE_THEME = 'theme';

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The theme handler service.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * The library discovery service.
   *
   * @var \Drupal\Core\Asset\LibraryDiscoveryInterface
   */
  protected $libraryDiscovery;

  /**
   * All libraries on the site.
   *
   * @var array
   */
  protected $libraries;

  /**
   * LibraryService constructor.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $themeHandler
   *   The theme handler.
   * @param \Drupal\Core\Asset\LibraryDiscoveryInterface $libraryDiscovery
   *   The library discovery service.
   */
  public function __construct(
    ModuleHandlerInterface $moduleHandler,
    ThemeHandlerInterface $themeHandler,
    LibraryDiscoveryInterface $libraryDiscovery
  ) {
    $this->moduleHandler = $moduleHandler;
    $this->libraryDiscovery = $libraryDiscovery;
    $this->themeHandler = $themeHandler;
  }

  /**
   * Gets all libraries.
   *
   * @return array
   *   The libraries.
   */
  public function getLibraries(): array {
    if (!isset($this->libraries)) {
      $this->buildLibraries();
    }
    return $this->libraries;
  }

  /**
   * Setter for libraries.
   *
   * @param array $libraries
   *   The libraries.
   */
  protected function setLibraries(array $libraries): void {
    $this->libraries = $libraries;
  }

  /**
   * Builds the internal list of libraries.
   */
  protected function buildLibraries(): void {
    $libraries = [
      'full_list' => [],
    ];
    foreach ($this::getLibraryTypes() as $type) {
      $libraries[$type] = [];
    }
    /** @var \Drupal\Core\Extension\Extension[] $extensions */
    $extensions = array_merge(
      $this->moduleHandler->getModuleList(),
      $this->themeHandler->rebuildThemeData()
    );
    foreach ($extensions as $extension) {
      $extensionName = $extension->getName();
      $extensionPath = $extension->getPath();
      $extensionType = $extension->getType();
      $type = $this->isCore($extensionPath) ? 'core_' . $extensionType : $extensionType;
      $extensionLibraries = $this->libraryDiscovery->getLibrariesByExtension($extensionName);
      if (empty($extensionLibraries)) {
        continue;
      }
      $libraries[$type][$extensionName] = $extensionLibraries;
      foreach ($extensionLibraries as $name => $data) {
        $libraries['full_list'][] = [
          'name' => $extensionName . '/' . $name,
          'type' => $this->getLibraryTypeReadable($type),
        ];
      }
    }
    // Add core libraries.
    $coreLibraries = $this->libraryDiscovery->getLibrariesByExtension('core');
    $libraries[static::LIBRARY_TYPE_CORE_MODULE]['core'] = $coreLibraries;
    foreach ($coreLibraries as $name => $data) {
      $libraries['full_list'][] = [
        'name' => "core/$name",
        'type' => $this->getLibraryTypeReadable(static::LIBRARY_TYPE_CORE_MODULE),
      ];
    }
    $this->setLibraries($libraries);
  }

  /**
   * Gets the simple list of libraries.
   *
   * @return array
   *   List of all libraries on site.
   */
  public function getLibrarySimpleList(): array {
    return $this->getLibraries()['full_list'];
  }

  /**
   * If the library is a core Drupal library or not.
   *
   * @param string $path
   *   The library path.
   *
   * @return bool
   *   If the library is a core Drupal library.
   */
  protected function isCore(string $path): bool {
    return strpos($path, 'core') === 0;
  }

  /**
   * Checks if the library exists.
   *
   * @param string $libraryName
   *   The full library name to check.
   *
   * @return bool
   *   If the library is exists.
   */
  public function isValidLibrary(string $libraryName): bool {
    $parts = explode('/', $libraryName);
    if (count($parts) !== 2) {
      return FALSE;
    }
    return is_array(
      $this->libraryDiscovery->getLibraryByName($parts[0], $parts[1])
    );
  }

  /**
   * Checks if the library is a library_field_entity.
   *
   * @param string $libraryName
   *   The library name.
   *
   * @return bool
   *   If this is a custom library entity.
   */
  public function isLibraryEntity(string $libraryName): bool {
    if (strpos($libraryName, static::LIBRARY_ENTITY_NAME . '/') === 0) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Gets the type of libraries.
   *
   * @return array
   *   List of library types.
   */
  public static function getLibraryTypes(): array {
    return [
      static::LIBRARY_TYPE_CORE_THEME,
      static::LIBRARY_TYPE_CORE_MODULE,
      static::LIBRARY_TYPE_MODULE,
      static::LIBRARY_TYPE_THEME,
    ];
  }

  /**
   * Gets the label for a library type.
   *
   * @param string $type
   *   The library type to get the label for.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The label of the library type.
   */
  public function getLibraryTypeReadable(string $type): TranslatableMarkup {
    $translations = [
      static::LIBRARY_TYPE_CORE_THEME => $this->t('Core Theme'),
      static::LIBRARY_TYPE_CORE_MODULE => $this->t('Core Module'),
      static::LIBRARY_TYPE_MODULE => $this->t('Module'),
      static::LIBRARY_TYPE_THEME => $this->t('Theme'),
    ];
    return $translations[$type];
  }

}
