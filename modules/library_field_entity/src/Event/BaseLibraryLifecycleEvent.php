<?php

declare(strict_types=1);

namespace Drupal\library_field_entity\Event;

use Symfony\Contracts\EventDispatcher\Event;
use Drupal\library_field_entity\Entity\LibraryFieldEntityInterface;

/**
 * A Base Library Event.
 */
abstract class BaseLibraryLifecycleEvent extends Event {

  /**
   * The library entity.
   *
   * @var \Drupal\library_field_entity\Entity\LibraryFieldEntityInterface
   */
  protected $library;

  /**
   * BaseLibraryLifecycleEvent constructor.
   *
   * @param \Drupal\library_field_entity\Entity\LibraryFieldEntityInterface $library
   *   The library entity.
   */
  public function __construct(LibraryFieldEntityInterface $library) {
    $this->library = $library;
  }

  /**
   * Getter for the Library Entity.
   *
   * @return \Drupal\library_field_entity\Entity\LibraryFieldEntityInterface
   *   The library entity.
   */
  public function getLibrary(): LibraryFieldEntityInterface {
    return $this->library;
  }

}
