<?php

declare(strict_types=1);

namespace Drupal\library_field_entity\Event;

/**
 * This event is dispatched when a Library is approved to be used.
 */
class LibraryApprovalEvent extends BaseLibraryLifecycleEvent {

  /**
   * The approved library event.
   */
  const NAME = 'library_field_entity.approval';

}
