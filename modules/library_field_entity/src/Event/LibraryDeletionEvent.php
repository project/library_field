<?php

declare(strict_types=1);

namespace Drupal\library_field_entity\Event;

/**
 * This event is dispatched when a library entity is deleted.
 */
class LibraryDeletionEvent extends BaseLibraryLifecycleEvent {

  /**
   * The delete event name.
   */
  const NAME = 'library_field_entity.delete';

}
