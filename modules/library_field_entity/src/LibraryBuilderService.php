<?php

declare(strict_types=1);

namespace Drupal\library_field_entity;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Builds the library entities information for dynamic library hook info.
 */
class LibraryBuilderService {

  /**
   * The library file service.
   *
   * @var LibraryFileService
   */
  protected $fileService;

  /**
   * Library storage.
   *
   * @var \Drupal\Core\Entity\RevisionableStorageInterface
   */
  protected $libraryStorage;

  /**
   * LibraryBuilderService constructor.
   *
   * @param LibraryFileService $fileService
   *   The library file service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(LibraryFileService $fileService, EntityTypeManagerInterface $entityTypeManager) {
    $this->fileService = $fileService;
    $this->libraryStorage = $entityTypeManager->getStorage('library_field_entity');
  }

  /**
   * Gets the libraries for build info hook.
   */
  public function getLibraries(): array {
    $libraries = [];
    $allLibraries = $this->libraryStorage->loadMultiple();
    foreach ($allLibraries as $library) {
      /** @var \Drupal\library_field_entity\Entity\LibraryFieldEntityInterface $library */
      if (!$library->isApproved() && empty($library->getRecentApprovedRevision())) {
        continue;
      }
      elseif ($library->isApproved()) {
        $libraries[$library->getName()] = $this->fileService->getLibraryInfo($library);
      }
      else {
        /** @var \Drupal\library_field_entity\Entity\LibraryFieldEntityInterface $currentActiveLibrary */
        $currentActiveLibrary = $this->libraryStorage->loadRevision($library->getRecentApprovedRevision());
        if (empty($currentActiveLibrary)) {
          return [];
        }
        $libraries[$currentActiveLibrary->getName()] = $this->fileService->getLibraryInfo($currentActiveLibrary);
      }
    }
    return $libraries;
  }

}
