<?php

declare(strict_types=1);

namespace Drupal\library_field_entity\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\library_field_entity\Entity\LibraryFieldEntityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The Library Field Entity Form.
 */
class LibraryFieldEntityForm extends ContentEntityForm {

  /**
   * The library name field key.
   */
  const NAME_FIELD = 'name';

  /**
   * The library entity settings config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The Library storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $libraryStorage;

  /**
   * LibraryFieldEntityForm constructor.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The configuration factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface|null $entity_type_bundle_info
   *   The entity type bundle info.
   * @param \Drupal\Component\Datetime\TimeInterface|null $time
   *   The time service.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(
    EntityRepositoryInterface $entity_repository,
    ConfigFactoryInterface $configFactory,
    EntityTypeManagerInterface $entityTypeManager,
    ?EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL,
    ?TimeInterface $time = NULL
  ) {
    $this->config = $configFactory->get('library_field_entity.settings');
    $this->libraryStorage = $entityTypeManager->getStorage('library_field_entity');
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('entity.repository'),
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    /** @var \Drupal\library_field_entity\Entity\LibraryFieldEntityInterface $entity */
    $entity = $this->entity;

    $form['revision_log']['#weight'] = 40;
    if (!$this->config->get('allow_js')) {
      $form['field_javascript_code']['#access'] = TRUE;
      $form['field_external_js_library']['#access'] = TRUE;
    }
    elseif (!$this->isJavaScriptUser()) {
      $form['field_javascript_code']['#disabled'] = TRUE;
      $form['field_external_js_library']['#disabled'] = TRUE;
    }
    // We don't need to check review flow.
    if ($this->entity->isNew()) {
      $form['revision_log']['#access'] = FALSE;
      return $form;
    }
    $form['name']['#disabled'] = TRUE;
    if (!$entity->doEditState()) {
      $this->messenger()->addError(
        $this->t("The current Library requires a review and cannot be edited")
      );
      $form['actions']['#disabled'] = TRUE;
      return $form;
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\library_field_entity\Entity\LibraryFieldEntityInterface $entity */
    $entity = $this->entity;
    $name = $form_state->getValue([static::NAME_FIELD, 0, 'value']);
    if ($entity->isNew()) {
      if (!preg_match('/^[a-zA-Z]+([_\-.][a-zA-Z]+)*$/', $name)) {
        $form_state->setErrorByName(
          static::NAME_FIELD,
          $this->t(
            "Only letters are allowed. Underscores, periods, and hyphens are allowed between letters (e.g. sample_library_name)"
          )
        );
      }
      elseif (!empty($this->libraryStorage->loadByProperties(['name' => $name]))) {
        $form_state->setErrorByName(
          static::NAME_FIELD,
          $this->t(
            "This library name already exists please choose a different one"
          )
        );
      }
    }
    else {
      if (!$entity->doEditState()) {
        $form_state->setErrorByName(
          static::NAME_FIELD,
          $this->t(
            "The current Library requires a review and cannot be edited"
          )
        );
      }
    }
    return parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\library_field_entity\Entity\LibraryFieldEntity $entity */
    $entity = $this->entity;
    $currentUserId = (int) $this->currentUser()->id();
    $entity
      ->setEditorId($currentUserId)
      ->setReviewState(LibraryFieldEntityInterface::REVIEW_STATE)
      ->setRevisionUserId($currentUserId);
    if ($entity->isNew()) {
      $entity->setAuthorId($currentUserId);
    }
    else {
      $entity->setApproved(FALSE);
    }
    $status = parent::save($form, $form_state);
    if ($status === SAVED_NEW) {
      $this->messenger()->addMessage(
        $this->t(
          "Created new Library: @name This library needs review before being active",
          [
            '@name' => $entity->getName(),
          ]
        )
      );
    }
    else {
      $this->messenger()->addMessage(
        $this->t(
          "Updated Library: @name This library needs review before this version is active",
          [
            '@name' => $entity->getName(),
          ]
        )
      );
    }
    $form_state->setRedirect(
      'entity.library_field_entity.canonical',
      [
        'library_field_entity' => $entity->id(),
      ]
    );
  }

  /**
   * Checks if the current user can use Javascript fields.
   *
   * @return bool
   *   TRUE if they can use JS FALSE if they cannot.
   */
  protected function isJavaScriptUser(): bool {
    $user = $this->currentUser();
    // Admins can use any field.
    if ($user->hasPermission('administer library_field_entity')) {
      return TRUE;
    }
    return $user->hasPermission('javascript library_field_entity');
  }

}
