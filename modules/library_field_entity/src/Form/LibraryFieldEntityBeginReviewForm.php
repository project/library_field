<?php

declare(strict_types=1);

namespace Drupal\library_field_entity\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\library_field_entity\Entity\LibraryFieldEntityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The library entity begin review form that lets a reviewer being a review.
 */
class LibraryFieldEntityBeginReviewForm extends ConfirmFormBase {

  /**
   * The library entity.
   *
   * @var \Drupal\library_field_entity\Entity\LibraryFieldEntity
   */
  protected $library;

  /**
   * The Library Field Entity settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * LibraryFieldEntityBeginReviewForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The Configuration Factory.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The Time service.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    TimeInterface $time
  ) {
    $this->config = $configFactory->get('library_field_entity.settings');
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('config.factory'),
      $container->get('datetime.time')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'library_field_entity_begin_review_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state,
    LibraryFieldEntityInterface $library_field_entity = NULL
  ) {
    $this->library = $library_field_entity;
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromUri('internal:/admin/content/library-field-entities/review');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t(
      'Do you want to begin a review of Library: @name ?',
      [
        '@name' => $this->library->getFullName(),
      ]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->library->setNewRevision();
    $this->library
      ->setReviewerId((int) $this->currentUser()->id())
      ->setReviewState(LibraryFieldEntityInterface::PENDING_STATE)
      ->setRevisionCreationTime($this->time->getRequestTime())
      ->save();
    $form_state->setRedirect(
      'entity.library_field_entity.review',
      [
        'library_field_entity' => $this->library->id(),
      ]
    );
  }

  /**
   * Access handler for this form.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user accessing the form.
   * @param \Drupal\library_field_entity\Entity\LibraryFieldEntityInterface $library_field_entity
   *   The library entity.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function access(
    AccountInterface $account,
    LibraryFieldEntityInterface $library_field_entity
  ): AccessResult {
    if ($library_field_entity->getReviewState() !== LibraryFieldEntityInterface::REVIEW_STATE) {
      return AccessResult::forbidden();
    }
    if ($account->hasPermission('administer library_field_entity')) {
      return AccessResult::allowed();
    }
    if ($account->hasPermission('review library_field_entity')) {
      if ($this->config->get('allow_author_approve')) {
        return AccessResult::allowed();
      }
      else {
        return AccessResult::allowedIf(
          $library_field_entity->getEditorId() !== (int) $account->id()
        );
      }
    }
    return AccessResult::forbidden();
  }

}
