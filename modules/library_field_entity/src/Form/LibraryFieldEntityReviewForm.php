<?php

declare(strict_types=1);

namespace Drupal\library_field_entity\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\library_field_entity\Entity\LibraryFieldEntityInterface;
use Drupal\library_field_entity\Event\LibraryApprovalEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * The library review form.
 */
class LibraryFieldEntityReviewForm extends FormBase {

  /**
   * The library entity.
   *
   * @var \Drupal\library_field_entity\Entity\LibraryFieldEntity
   */
  protected $library;

  /**
   * The library configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $dispatcher;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * LibraryFieldEntityReviewForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The configuration factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher
   *   The event dispatcher.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    EntityTypeManagerInterface $entityTypeManager,
    EventDispatcherInterface $dispatcher,
    TimeInterface $time
  ) {
    $this->config = $configFactory->get('library_field_entity.settings');
    $this->entityTypeManager = $entityTypeManager;
    $this->dispatcher = $dispatcher;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('event_dispatcher'),
      $container->get('datetime.time')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state,
    LibraryFieldEntityInterface $library_field_entity = NULL
  ) {
    $this->library = $library_field_entity;
    $form['entity_view'] = $this->entityTypeManager
      ->getViewBuilder('library_field_entity')
      ->view($this->library);
    $form['actions'] = [
      'reject' => [
        '#name' => 'reject',
        '#type' => 'submit',
        '#value' => $this->t('Reject'),
      ],
      'approve' => [
        '#name' => 'approve',
        '#type' => 'submit',
        '#value' => $this->t('Approve'),
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'library_field_entity_review_form';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    if ($trigger['#name'] === 'approve') {
      $this->library->setNewRevision();
      $this->library
        ->setReviewState(LibraryFieldEntityInterface::APPROVED_STATE)
        ->setRevisionUserId((int) $this->currentUser()->id())
        ->setApproved(TRUE)
        ->setRevisionCreationTime($this->time->getRequestTime())
        ->save();
      $this->library->setNewRevision(FALSE);
      $this->library
        ->setRecentApprovedRevision((int) $this->library->getRevisionId())
        ->save();
      $this->dispatcher->dispatch(new LibraryApprovalEvent($this->library), LibraryApprovalEvent::NAME);
      $this->messenger()->addMessage(
        $this->t(
          "Successfully approved and activated Library: @name",
          ['@name' => $this->library->getFullName()]
        )
      );
    }
    else {
      $this->library->setNewRevision();
      $this->library
        ->setReviewState(LibraryFieldEntityInterface::REJECTED_STATE)
        ->setRevisionUserId((int) $this->currentUser()->id())
        ->save();
      $this->messenger()->addWarning(
        $this->t(
          "Rejected Library: @name",
          ['@name' => $this->library->getFullName()]
        )
          );
    }
    $form_state->setRedirect(
      'entity.library_field_entity.review-view',
    );
  }

  /**
   * The access handler.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user account checking access to form.
   * @param \Drupal\library_field_entity\Entity\LibraryFieldEntityInterface $library_field_entity
   *   The library field entity.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function access(
    AccountInterface $account,
    LibraryFieldEntityInterface $library_field_entity
  ): AccessResult {
    if ($library_field_entity->getReviewState() !== LibraryFieldEntityInterface::PENDING_STATE) {
      return AccessResult::forbidden();
    }
    if ($account->hasPermission('administer library_field_entity')) {
      return AccessResult::allowed();
    }
    if ($account->hasPermission('review library_field_entity')) {
      if ($this->config->get('allow_author_approve')) {
        return AccessResult::allowedIf($library_field_entity->getReviewerId() === (int) $account->id());
      }
      else {
        return AccessResult::allowedIf(
          $library_field_entity->getEditorId() !== (int) $account->id() && $library_field_entity->getReviewerId() === (int) $account->id()
        );
      }
    }
    return AccessResult::forbidden();
  }

}
