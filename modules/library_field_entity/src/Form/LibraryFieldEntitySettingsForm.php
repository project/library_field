<?php

declare(strict_types=1);

namespace Drupal\library_field_entity\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * The library field entity settings form.
 */
class LibraryFieldEntitySettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['library_field_entity.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'library_field_entity_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('library_field_entity.settings');
    $form['allow_js'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow JavaScript code'),
      '#default_value' => $config->get('allow_js'),
    ];
    $form['allow_author_approve'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow Authors of an Entity to Approve their own Entities'),
      '#default_value' => $config->get('allow_author_approve'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('library_field_entity.settings')
      ->set('allow_js', (bool) $form_state->getValue('allow_js'))
      ->save();
    $this->config('library_field_entity.settings')
      ->set('allow_author_approve', (bool) $form_state->getValue('allow_author_approve'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
