<?php

declare(strict_types=1);

namespace Drupal\library_field_entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Drupal\library_field_entity\Controller\LibraryFieldEntityController;
use Drupal\library_field_entity\Form\LibraryFieldEntitySettingsForm;
use Symfony\Component\Routing\Route;

/**
 * Provides routes for Library entities.
 */
class LibraryFieldEntityHtmlRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);
    $entity_type_id = $entity_type->id();
    $adminPermission = $entity_type->getAdminPermission();

    $revisionRoute = new Route($entity_type->getLinkTemplate('revision'));
    $revisionRoute
      ->setDefaults([
        '_controller' => LibraryFieldEntityController::class . '::revisionDisplay',
        '_title_callback' => LibraryFieldEntityController::class . '::revisionTitle',
      ])
      ->setRequirement('_permission', 'review library_field_entity')
      ->setOption('_admin_route', TRUE);
    $collection->add("entity.{$entity_type_id}.revision", $revisionRoute);

    $settingsRoute = new Route("/admin/structure/library-field-entity/settings");
    $settingsRoute
      ->setDefaults([
        '_form' => LibraryFieldEntitySettingsForm::class,
        '_title' => "{$entity_type->getLabel()} Settings",
      ])
      ->setRequirement('_permission', $adminPermission)
      ->setOption('_admin_route', TRUE);
    $collection->add("{$entity_type_id}.settings", $settingsRoute);

    $revisionsRoute = new Route($entity_type->getLinkTemplate('revisions'));
    $collection->add("entity.{$entity_type_id}.revisions", $revisionsRoute);

    $activeRoute = new Route($entity_type->getLinkTemplate('active-view'));
    $collection->add("entity.{$entity_type_id}.active-view", $activeRoute);

    $reviewRoute = new Route($entity_type->getLinkTemplate('review-view'));
    $collection->add("entity.{$entity_type_id}.review-view", $reviewRoute);

    $manageRoute = new Route($entity_type->getLinkTemplate('manage'));
    $collection->add("entity.{$entity_type_id}.manage", $manageRoute);

    $collection
      ->get("entity.{$entity_type_id}.collection")
      ->setRequirement('_permission', "review library_field_entity+$adminPermission+create and update library_field_entity");

    return $collection;
  }

}
