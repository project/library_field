<?php

declare(strict_types=1);

namespace Drupal\library_field_entity\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Controller for Library Field Entities.
 */
class LibraryFieldEntityController extends ControllerBase {

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Library storage.
   *
   * @var \Drupal\Core\Entity\RevisionableStorageInterface
   */
  protected $libraryStorage;

  /**
   * LibraryFieldEntityController constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $dateFormatter
   *   The date formatter.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    DateFormatterInterface $dateFormatter
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->dateFormatter = $dateFormatter;
    $this->libraryStorage = $entityTypeManager->getStorage('library_field_entity');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('date.formatter')
    );
  }

  /**
   * Displays a revision.
   *
   * @param int|string $library_field_entity_revision
   *   The revision ID.
   *
   * @return array
   *   The render array of the revision.
   */
  public function revisionDisplay($library_field_entity_revision) {
    $library = $this->libraryStorage->loadRevision($library_field_entity_revision);
    if (empty($library)) {
      throw new BadRequestHttpException("Unknown Library revision");
    }
    return $this->entityTypeManager
      ->getViewBuilder('library_field_entity')
      ->view($library);
  }

  /**
   * Gets the revision title.
   *
   * @param int|string $library_field_entity_revision
   *   The revision ID.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The title.
   */
  public function revisionTitle($library_field_entity_revision) {
    $library = $this->libraryStorage->loadRevision($library_field_entity_revision);
    if (empty($library)) {
      throw new BadRequestHttpException("Unknown Library revision");
    }
    return $this->t(
      "Library: `@name` from @date",
      [
        '@name' => $library->getName(),
        '@date' => $this->dateFormatter->format($library->getRevisionCreationTime()),
      ]
    );
  }

}
