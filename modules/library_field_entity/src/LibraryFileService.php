<?php

declare(strict_types=1);

namespace Drupal\library_field_entity;

use Drupal\Core\File\Exception\FileWriteException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\library_field\LibraryService;
use Drupal\library_field_entity\Entity\LibraryFieldEntityInterface;
use Drupal\Core\StreamWrapper\PublicStream;

/**
 * The library file and info service.
 */
class LibraryFileService {

  /**
   * The name for JavaScript code type and paths.
   */
  const JS_TYPE = 'js';

  /**
   * The name for CSS code type and paths.
   */
  const CSS_TYPE = 'css';

  /**
   * The base directory for assets.
   */
  const BASE_DIR = 'library_field_entity';

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The library service.
   *
   * @var \Drupal\library_field\LibraryService
   */
  protected $libraryService;

  /**
   * The application root.
   *
   * @var string
   */
  protected $appRoot;

  /**
   * LibraryFileService constructor.
   *
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   File system.
   * @param \Drupal\library_field\LibraryService $libraryService
   *   Library service.
   * @param string $appRoot
   *   Application root.
   */
  public function __construct(
    FileSystemInterface $fileSystem,
    LibraryService $libraryService,
    string $appRoot
  ) {
    $this->fileSystem = $fileSystem;
    $this->libraryService = $libraryService;
    $this->appRoot = $appRoot;
  }

  /**
   * Getter for the base library URI.
   *
   * @return string
   *   The base URI.
   */
  public static function getBaseUri(): string {
    return 'public://' . static::BASE_DIR;
  }

  /**
   * Writes and removes library files based on entity passed.
   *
   * @param \Drupal\library_field_entity\Entity\LibraryFieldEntityInterface $library
   *   Library entity.
   */
  public function writeLibraryFiles(LibraryFieldEntityInterface $library): void {
    $directory = $this::getBaseUri();
    $baseJs = $this::getBaseUri() . '/' . static::JS_TYPE;
    $baseCss = $this::getBaseUri() . '/' . static::CSS_TYPE;
    $bitmask = FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS;
    if (
      !$this->fileSystem->prepareDirectory($directory, $bitmask)
      || !$this->fileSystem->prepareDirectory($baseJs, $bitmask)
      || !$this->fileSystem->prepareDirectory($baseCss, $bitmask)
    ) {
      throw new FileWriteException("Unable to write to directory: $directory");
    }
    $allData = [
      static::JS_TYPE => $library->getJsData(),
      static::CSS_TYPE => $library->getCssData(),
    ];
    foreach ($allData as $type => $data) {
      if (!empty($data)) {
        $uri = $this->getDataUri($type, $library, $this::getBaseUri());
        $this->fileSystem->saveData($data, $uri, FileSystemInterface::EXISTS_REPLACE);
      }
      else {
        // The data is empty so make sure to cleanup any files.
        $this->deleteLibraryFile($type, $library);
      }
    }
  }

  /**
   * Deletes specific library assets from the file system based on type.
   *
   * @param string $type
   *   The asset type to delete.
   * @param \Drupal\library_field_entity\Entity\LibraryFieldEntityInterface $library
   *   The library entity to execute file deletion.
   */
  protected function deleteLibraryFile(string $type, LibraryFieldEntityInterface $library): void {
    $path = $this->getDataFilePath($type, $library);
    $this->fileSystem->delete($path);
  }

  /**
   * Deletes all assets associated with a library entity.
   *
   * @param \Drupal\library_field_entity\Entity\LibraryFieldEntityInterface $library
   *   The library entity to execute file deletion.
   */
  public function deleteAllLibraryFiles(LibraryFieldEntityInterface $library): void {
    foreach ([static::CSS_TYPE, static::JS_TYPE] as $type) {
      $this->deleteLibraryFile($type, $library);
    }
  }

  /**
   * Gets the data URI of a specific asset type from a library entity.
   *
   * @param string $type
   *   The asset type.
   * @param \Drupal\library_field_entity\Entity\LibraryFieldEntityInterface $library
   *   The library entity.
   * @param string $base
   *   The base path.
   *
   * @return string
   *   The URI.
   */
  protected function getDataUri(string $type, LibraryFieldEntityInterface $library, string $base): string {
    if ($type !== static::CSS_TYPE && $type !== static::JS_TYPE) {
      throw new \InvalidArgumentException("Unrecognized type entered");
    }
    return implode("/", [
      $base,
      $type,
      $library->getName() . '.' . $type,
    ]);
  }

  /**
   * Gets the real path of the asset type.
   *
   * @param string $type
   *   The asset type.
   * @param \Drupal\library_field_entity\Entity\LibraryFieldEntityInterface $library
   *   The library entity.
   *
   * @return string
   *   The path to the asset file.
   */
  public function getDataFilePath(string $type, LibraryFieldEntityInterface $library): string {
    return $this->getDataUri(
      $type,
      $library,
      $this->appRoot . '/' . PublicStream::basePath() . '/' . static::BASE_DIR
    );
  }

  /**
   * Gets the application relative path of the asset type.
   *
   * @param string $type
   *   The asset type.
   * @param \Drupal\library_field_entity\Entity\LibraryFieldEntityInterface $library
   *   The library entity.
   *
   * @return string
   *   The relative path to app root.
   */
  public function getDataLibraryPath(string $type, LibraryFieldEntityInterface $library): string {
    return $this->getDataUri(
      $type,
      $library,
      '/' . PublicStream::basePath() . '/' . static::BASE_DIR
    );
  }

  /**
   * Builds the library information needed for library build info hook.
   *
   * @param \Drupal\library_field_entity\Entity\LibraryFieldEntityInterface $library
   *   The library entity.
   *
   * @return array
   *   The library data, can be empty.
   */
  public function getLibraryInfo(LibraryFieldEntityInterface $library): array {
    $info = [];
    $dependencies = $library->getLibraries();
    if ($dependencies) {
      array_filter($dependencies, function ($dependency) use ($library) {
        if ($dependency !== $library->getFullName()) {
          return $this->libraryService->isValidLibrary($dependency);
        }
        return FALSE;
      });
    }
    if ($dependencies) {
      $info['dependencies'] = $dependencies;
    }
    $css = [];
    if ($cssExternal = $library->getExternalCssLibraries()) {
      $css['base'] = [];
      foreach ($cssExternal as $path) {
        $css['base'][$path] = ['external' => TRUE];
      }
    }
    if (!empty($library->getCssData())) {
      $css['theme'] = [
        $this->getDataLibraryPath(static::CSS_TYPE, $library) => [],
      ];
    }
    if (!empty($css)) {
      $info['css'] = $css;
    }
    $js = [];
    if ($jsExternal = $library->getExternalJsLibraries()) {
      foreach ($jsExternal as $path) {
        $js[$path] = ['external' => TRUE];
      }
    }
    if (!empty($library->getJsData())) {
      $js[$this->getDataLibraryPath(static::JS_TYPE, $library)] = [];
    }
    if (!empty($js)) {
      $info['js'] = $js;
    }
    if (!empty($info)) {
      $info['version'] = (int) $library->getRevisionId();
    }
    return $info;
  }

}
