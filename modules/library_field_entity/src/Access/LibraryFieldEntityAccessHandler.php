<?php

declare(strict_types=1);

namespace Drupal\library_field_entity\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access handler for Library Field Entities.
 */
class LibraryFieldEntityAccessHandler extends EntityAccessControlHandler {

  /**
   * The "view" operation.
   */
  public const VIEW_OPERATION = 'view';

  /**
   * The "update" operation.
   */
  public const UPDATE_OPERATION = 'update';

  /**
   * The "delete" operation.
   */
  public const DELETE_OPERATION = 'delete';

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(
    EntityInterface $entity,
    $operation,
    AccountInterface $account
  ) {
    /** @var \Drupal\library_field_entity\Entity\LibraryFieldEntityInterface $entity */
    $adminPermission = $this->entityType->getAdminPermission();
    if ($account->hasPermission($adminPermission)) {
      return AccessResult::allowed();
    }
    switch ($operation) {
      case static::VIEW_OPERATION:
        if ($account->hasPermission('review library_field_entity')) {
          return AccessResult::allowed();
        }
        if ($account->hasPermission('edit any library_field_entity')) {
          return AccessResult::allowed();
        }
        if ($account->hasPermission('delete any library_field_entity')) {
          return AccessResult::allowed();
        }
        if ($account->hasPermission('view any library_field_entity')) {
          return AccessResult::allowed();
        }
        elseif ($account->hasPermission('create and update library_field_entity')) {
          return AccessResult::allowedIf($entity->getAuthorId() === (int) $account->id());
        }
        else {
          return AccessResult::forbidden();
        }

      case static::UPDATE_OPERATION:
        if ($account->hasPermission('edit any library_field_entity')) {
          return AccessResult::allowed();
        }
        elseif ($account->hasPermission('create and update library_field_entity')) {
          return AccessResult::allowedIf($entity->getAuthorId() === (int) $account->id());
        }
        else {
          return AccessResult::forbidden();
        }

      case static::DELETE_OPERATION:
        if ($account->hasPermission('delete any library_field_entity')) {
          return AccessResult::allowed();
        }
        elseif ($account->hasPermission('delete library_field_entity')) {
          return AccessResult::allowedIf($entity->getAuthorId() === (int) $account->id());
        }
        else {
          return AccessResult::forbidden();
        }
    }
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(
    AccountInterface $account,
    array $context,
    $entity_bundle = NULL
  ) {
    $adminPermission = $this->entityType->getAdminPermission();
    if ($account->hasPermission($adminPermission)) {
      return AccessResult::allowed();
    }
    return AccessResult::allowedIfHasPermission(
      $account,
      'create and update library_field_entity'
    );
  }

}
