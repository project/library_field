<?php

declare(strict_types=1);

namespace Drupal\library_field_entity\Entity;

use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the Library Field entity.
 *
 * @ingroup library_field
 *
 * @ContentEntityType(
 *   id = "library_field_entity",
 *   label = @Translation("Library Field Entity"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\Core\Entity\EntityListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\library_field_entity\Form\LibraryFieldEntityForm",
 *       "add" = "Drupal\library_field_entity\Form\LibraryFieldEntityForm",
 *       "edit" = "Drupal\library_field_entity\Form\LibraryFieldEntityForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *   "route_provider" = {
 *       "html" = "Drupal\library_field_entity\LibraryFieldEntityHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\library_field_entity\Access\LibraryFieldEntityAccessHandler",
 *   },
 *   base_table = "library_field_entity",
 *   translatable = FALSE,
 *   fieldable = TRUE,
 *   revision_table = "library_field_entity_revision",
 *   revision_data_table = "library_field_entity_data_revision",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "label" = "name",
 *     "revision" = "vid",
 *     "uid" = "user_id"
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log"
 *   },
 *   links = {
 *     "canonical" = "/admin/content/library-field-entity/{library_field_entity}",
 *     "add-form" = "/admin/content/library-field-entity/add",
 *     "edit-form" = "/admin/content/library-field-entity/{library_field_entity}/edit",
 *     "delete-form" = "/admin/content/library-field-entity/{library_field_entity}/delete",
 *     "collection" = "/admin/content/library-field-entities",
 *     "revisions" = "/admin/structure/library-field-entity/{library_field_entity}/revisions",
 *     "revision" = "/admin/structure/library-field-entity/{library_field_entity}/revisions/{library_field_entity_revision}/view",
 *     "active-view" = "/admin/content/library-field-entities/active",
 *     "review-view" = "/admin/content/library-field-entities/review",
 *     "manage" = "/admin/structure/library-field-entities/manage"
 *   },
 *   admin_permission = "administer library_field_entity",
 *   field_ui_base_route = "library_field_entity.settings",
 *   internal = TRUE
 * )
 */
class LibraryFieldEntity extends RevisionableContentEntityBase implements LibraryFieldEntityInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function doEditState(): bool {
    return in_array(
      $this->getReviewState(),
      static::ALLOWED_EDIT_STATES
    );
  }

  /**
   * {@inheritdoc}
   */
  public function doReviewState(): bool {
    return in_array(
      $this->getReviewState(),
      static::ALLOWED_REVIEWER_STATES
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getExternalJsLibraries(): ?array {
    $fieldItems = $this->get('field_external_js_library');
    if ($fieldItems->isEmpty()) {
      return NULL;
    }
    return array_map(function ($data) {
      return $data['uri'];
    }, $fieldItems->getValue());
  }

  /**
   * {@inheritdoc}
   */
  public function getExternalCssLibraries(): ?array {
    $fieldItems = $this->get('field_external_css_libraries');
    if ($fieldItems->isEmpty()) {
      return NULL;
    }
    return array_map(function ($data) {
      return $data['uri'];
    }, $fieldItems->getValue());
  }

  /**
   * {@inheritdoc}
   */
  public function getCssData(): ?string {
    return $this->get('field_css_code')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getJsData(): ?string {
    return $this->get('field_javascript_code')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(): ?array {
    $fieldItems = $this->get('field_drupal_libraries');
    if ($fieldItems->isEmpty()) {
      return NULL;
    }
    return array_map(function ($data) {
      return $data['value'];
    }, $fieldItems->getValue());
  }

  /**
   * {@inheritdoc}
   */
  public function getName(): string {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getFullName(): string {
    return implode('/', ['library_field_entity', $this->getName()]);
  }

  /**
   * {@inheritdoc}
   */
  public function isApproved(): bool {
    return (bool) $this->get('approved')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setApproved(bool $value) {
    $this->set('approved', $value);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getReviewState(): string {
    return $this->get('review_state')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setReviewState(string $state) {
    if (!in_array($state, static::ALLOWED_REVIEW_STATES)) {
      throw new \InvalidArgumentException('Unknown review state');
    }
    $this->set('review_state', $state);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getRecentApprovedRevision(): ?int {
    return ((int) $this->get('last_approved_vid')->value) ?: NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setRecentApprovedRevision(int $vid) {
    $this->set('last_approved_vid', $vid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getEditorId(): ?int {
    return ((int) $this->get('user_id_editor')->target_id) ?: NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setEditorId(int $uid) {
    $this->set('user_id_editor', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getReviewerId(): ?int {
    return ((int) $this->get('reviewer')->target_id) ?: NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setReviewerId(?int $uid) {
    $this->set('reviewer', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getAuthorId(): int {
    return (int) $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setAuthorId(int $uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Library entity. Only lowercase letters and underscores are allowed.'))
      ->setRevisionable(FALSE)
      ->setSettings([
        'max_length' => 100,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setTranslatable(FALSE)
      ->setRequired(TRUE);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Author'))
      ->setDescription(t('The user ID of original author.'))
      ->setRevisionable(FALSE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setTranslatable(FALSE);

    $fields['user_id_editor'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Recent Editor'))
      ->setDescription(t('The user ID of the most recent editor.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setTranslatable(FALSE);

    $fields['reviewer'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Reviewer'))
      ->setDescription(t('The user ID of the most recent reviewer.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setTranslatable(FALSE);

    $fields['review_state'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Review State'))
      ->setDescription(t('The current review state of the library.'))
      ->setSettings([
        'allowed_values' => [
          'pending' => 'Pending',
          'approved' => 'Approved',
          'rejected' => 'Rejected',
          'review' => 'Needs Review',
        ],
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'list_default',
        'weight' => -4,
      ])
      ->setDefaultValue('review')
      ->setDisplayConfigurable('view', TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(FALSE);

    $fields['description'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Label'))
      ->setDescription(t('A label or quick description of this library.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setTranslatable(FALSE)
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -3,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['approved'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Approval status'))
      ->setDescription(t('A boolean indicating whether the Library is currently approved.'))
      ->setRevisionable(TRUE)
      ->setTranslatable(FALSE)
      ->setDefaultValue(FALSE);

    $fields['last_approved_vid'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Last approved vid'))
      ->setDescription(t('The most recent approved revision of the library.'))
      ->setRevisionable(TRUE)
      ->setTranslatable(FALSE)
      ->setRequired(FALSE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the Library entity was created.'))
      ->setRevisionable(FALSE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the Library entity was last edited.'))
      ->setRevisionable(TRUE);

    return $fields;
  }

}
