<?php

declare(strict_types=1);

namespace Drupal\library_field_entity\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * A Library Field Entity Interface.
 */
interface LibraryFieldEntityInterface extends ContentEntityInterface {

  /**
   * The library has been approved and can be used on this site.
   */
  const APPROVED_STATE = 'approved';

  /**
   * The library is being actively reviewed.
   */
  const PENDING_STATE = 'pending';

  /**
   * The library needs a reviewer.
   */
  const REVIEW_STATE = 'review';

  /**
   * The library has been rejected.
   */
  const REJECTED_STATE = 'rejected';

  /**
   * The allowed states a library can be in through its review lifecycle.
   */
  const ALLOWED_REVIEW_STATES = [
    'approved',
    'pending',
    'review',
    'rejected',
  ];

  /**
   * Allowed states to edit a library.
   */
  const ALLOWED_EDIT_STATES = [
    'rejected',
    'approved',
  ];

  /**
   * Allowed states to begin a review of a library.
   */
  const ALLOWED_REVIEWER_STATES = [
    'pending',
    'review',
  ];

  /**
   * If the current library can be edited.
   *
   * @return bool
   *   If the library can be edited.
   */
  public function doEditState(): bool;

  /**
   * If the current library is under or needs review.
   *
   * @return bool
   *   If the library is in a review state.
   */
  public function doReviewState(): bool;

  /**
   * Getter for external JavaScript libraries.
   *
   * @return array|null
   *   Returns an array of links or null if there are none.
   */
  public function getExternalJsLibraries(): ?array;

  /**
   * Getter for external CSS libraries.
   *
   * @return array|null
   *   Returns an array of links or null if there are none.
   */
  public function getExternalCssLibraries(): ?array;

  /**
   * Getter for CSS data.
   *
   * @return string
   *   The CSS code.
   */
  public function getCssData(): ?string;

  /**
   * Getter for JavaScript data.
   *
   * @return string
   *   The Javascript code.
   */
  public function getJsData(): ?string;

  /**
   * Getter for Drupal libraries.
   *
   * @return array|null
   *   The Libraries or null if there are none.
   */
  public function getLibraries(): ?array;

  /**
   * Getter for Library name.
   *
   * @return string
   *   The library name.
   */
  public function getName(): string;

  /**
   * Getter for Full Library name.
   *
   * @return string
   *   The full name of the library.
   */
  public function getFullName(): string;

  /**
   * If the Library is approved.
   *
   * @return bool
   *   If the library is approved.
   */
  public function isApproved(): bool;

  /**
   * Setter for the Library.
   *
   * @param bool $value
   *   Approved or Not.
   *
   * @return self
   *   itself.
   */
  public function setApproved(bool $value);

  /**
   * Getter for the review state.
   *
   * @return string
   *   The current review state.
   *
   * @see ALLOWED_REVIEW_STATES
   */
  public function getReviewState(): string;

  /**
   * Setter for the Review State.
   *
   * @param string $state
   *   The current review state.
   *
   * @return self
   *   itself.
   *
   * @see ALLOWED_REVIEW_STATES
   */
  public function setReviewState(string $state);

  /**
   * Getter for the most recently approved revision ID.
   *
   * @return int|null
   *   The VID or null if there is none.
   */
  public function getRecentApprovedRevision(): ?int;

  /**
   * Setter for the most recently approved VID.
   *
   * @param int $vid
   *   The VID that is approved for site usage.
   *
   * @return self
   *   itself.
   */
  public function setRecentApprovedRevision(int $vid);

  /**
   * Getter for the editor ID.
   *
   * @return int|null
   *   This is the most recent user to edit the entity can be empty.
   */
  public function getEditorId(): ?int;

  /**
   * Setter for the editor ID.
   *
   * @param int $uid
   *   The ID of the user editing the entity.
   *
   * @return self
   *   itself.
   */
  public function setEditorId(int $uid);

  /**
   * Getter for the reviewer's ID.
   *
   * @return int|null
   *   The ID of the most recent reviewer.
   */
  public function getReviewerId(): ?int;

  /**
   * Setter for the reviewer ID.
   *
   * @param int|null $uid
   *   The current reviewer's uid can be empty.
   *
   * @return self
   *   itself.
   */
  public function setReviewerId(?int $uid);

  /**
   * Getter for the Author ID.
   *
   * @return int
   *   The Author's ID.
   */
  public function getAuthorId(): int;

  /**
   * Setter for the Author's ID.
   *
   * @param int $uid
   *   The ID of the Author.
   */
  public function setAuthorId(int $uid);

}
