<?php

declare(strict_types=1);

namespace Drupal\library_field_entity\EventSubscriber;

use Drupal\Core\Asset\LibraryDiscoveryInterface;
use Drupal\Core\Cache\Cache;
use Drupal\library_field\LibraryCacheTagUtil;
use Drupal\library_field_entity\Entity\LibraryFieldEntityInterface;
use Drupal\library_field_entity\Event\LibraryApprovalEvent;
use Drupal\library_field_entity\Event\LibraryDeletionEvent;
use Drupal\library_field_entity\LibraryFileService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * The Library Entity Lifecycle Event Subscriber.
 */
class LibraryLifecycleEventSubscriber implements EventSubscriberInterface {

  /**
   * The library file service.
   *
   * @var \Drupal\library_field_entity\LibraryFileService
   */
  protected $fileService;

  /**
   * The library discovery service.
   *
   * @var \Drupal\Core\Asset\LibraryDiscoveryInterface
   */
  protected $libraryDiscovery;

  /**
   * LibraryLifecycleEventSubscriber constructor.
   *
   * @param \Drupal\library_field_entity\LibraryFileService $fileService
   *   The library file service.
   * @param \Drupal\Core\Asset\LibraryDiscoveryInterface $libraryDiscovery
   *   The library discovery service.
   */
  public function __construct(
    LibraryFileService $fileService,
    LibraryDiscoveryInterface $libraryDiscovery
  ) {
    $this->fileService = $fileService;
    $this->libraryDiscovery = $libraryDiscovery;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      LibraryApprovalEvent::NAME => ['onApproval', 20],
      LibraryDeletionEvent::NAME => ['onDeletion', 20],
    ];
  }

  /**
   * Handles when a library is approved; writes new files.
   *
   * @param \Drupal\library_field_entity\Event\LibraryApprovalEvent $event
   *   The Library approved event.
   */
  public function onApproval(LibraryApprovalEvent $event): void {
    $library = $event->getLibrary();
    $this->fileService->writeLibraryFiles($library);
    $this->clearCache($library);
  }

  /**
   * Handles when a library is removed; removes files.
   *
   * @param \Drupal\library_field_entity\Event\LibraryDeletionEvent $event
   *   The library deletion event.
   */
  public function onDeletion(LibraryDeletionEvent $event): void {
    $library = $event->getLibrary();
    $this->fileService->deleteAllLibraryFiles($library);
    $this->clearCache($library);
  }

  /**
   * Clears the Library cache, and asset files.
   *
   * @param \Drupal\library_field_entity\Entity\LibraryFieldEntityInterface $library
   *   The library to clear cache for.
   */
  protected function clearCache(LibraryFieldEntityInterface $library): void {
    $this->libraryDiscovery->clearCachedDefinitions();
    Cache::invalidateTags(
      [
        LibraryCacheTagUtil::getTagFromLibrary($library->getFullName()),
        'library_info',
      ]
    );
    // Due to asset caching/aggregation this is a simple fix, may change.
    \Drupal::service('asset.css.collection_optimizer')->deleteAll();
    \Drupal::service('asset.js.collection_optimizer')->deleteAll();
    _drupal_flush_css_js();
  }

}
