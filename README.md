CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Contributing
* License
* Maintainers


INTRODUCTION
------------

The Library Field module provides a new field type that attaches libraries.
The module also provides a library overview page that lists all libraries
available. A custom block type for library fields is included that allows
attaching of libraries on a block, this block will be hidden.

There is a secondary sub module Library Field Entity that allows users to
create their own libraries, which must go through a review process internally.


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

Install the Library Field module as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.


CONFIGURATION
-------------

Library Field:

    1. Navigate to Administration > Extend and enable the Library Field module.
    2. Add the new "Library Attacher" field to any entity you would like it
       available.
    3. Make sure to hide the field label, and pick the "Attach libraries"
       formatter.
    4. You can also use a custom block type "Library Field" to attach libraries
       using blocks.
    5. There are permissions that allow a user to view libraries, and use the
       library field if it exists. Make sure to enable them otherwise you cannot
       use the field if you are not an administrator.

Library Field Entity:

    1. Navigate to Administration > Extend and enable the Library Field Entity
       module.
    2. Navigate to Administration > Configuration > Library Field Entity
       Settings to change the configuration.
    3. Navigate to Administration > Content > Library Field Entities to add
       an entity.
    4. Navigate to Administration > Structure > Manage Library Field Entites to
       manage and review Library Field Entities.

Configurable parameters (Library Field Entity):
* Allow / Exclude JavaScript code in library field entities.
* Allow / Exclude Authors of an Entity to Approve their own Entities, an admin
  can always bypass this rule.

Recommended Permissions (Library Field Entity):

    1. Admin - `administer library_field_entity`,
       `create and update library_field_entity`,
       and `review library_field_entity`
    2. Reviewer - `review library_field_entity`,
       `create and update library_field_entity`,
       and `delete any library_field_entity`
    3. Creator - `create and update library_field_entity`

CONTRIBUTING
------------

See CONTRIBUTING.md

LICENSES
--------

See LICENSE.md

MAINTAINERS
-----------

* Ben Holt - https://www.drupal.org/u/bits8mybytes
