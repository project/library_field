module.exports = {
  env: {
    browser: true,
    node: true,
    es6: true,
  },
  plugins: ["prettier"],
  extends: [
    'airbnb-base',
    'plugin:prettier/recommended'
  ],
  parserOptions: {
    ecmaVersion: 11,
  },
  globals: {
    "Drupal": true,
    "drupalSettings": true,
    "drupalTranslations": true,
    "domready": true,
    "jQuery": true,
    "_": true,
    "matchMedia": true,
    "Backbone": true,
    "Modernizr": true,
    "Popper": true,
    "Sortable": true,
    "CKEDITOR": true
  },
  rules: {
    "prettier/prettier": "error",
    "consistent-return": ["off"],
    "no-underscore-dangle": ["off"],
    "max-nested-callbacks": ["warn", 3],
    "import/no-mutable-exports": ["warn"],
    "no-plusplus": ["warn", {
      "allowForLoopAfterthoughts": true
    }],
    "no-param-reassign": ["off"],
    "no-prototype-builtins": ["off"],
    "valid-jsdoc": ["warn", {
      "prefer": {
        "returns": "return",
        "property": "prop"
      },
      "requireReturn": false
    }],
    "no-unused-vars": ["warn"],
    "operator-linebreak": ["error", "after", { "overrides": { "?": "ignore", ":": "ignore" } }]
  }
};
