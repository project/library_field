# General

Using phpcs for PHP see `phpcs.xml.dist`. Using yarn and webpack for
front end use `yarn build` to build assets. Using linters for JS and SCSS
use `yarn lint:js` and `yarn lint:css`. See `package.json` for more.
