/**
 * @file
 * Adds a modal library list for a library field.
 */
import MicroModal from "micromodal";
import List from "list.js";
import QuickList from "./quick-list.es6";

Drupal.behaviors.libraryFieldQuickView = {
  attach(context) {
    const currentModal = this.modal;
    if (currentModal === null) {
      return;
    }
    if (!this.hasInit) {
      this.loadModal(currentModal);
      this.hasInit = true;
    } else {
      MicroModal.init({
        awaitCloseAnimation: true,
      });
    }
    const listManager = this.list;
    const quickLinks = context.querySelectorAll("a[data-quick-list-delta]");
    quickLinks.forEach((element) => {
      element.addEventListener("click", (e) => {
        const delta = e.target.getAttribute("data-quick-list-delta");
        listManager.triggeringField = `data-quick-list-${delta}`;
      });
    });
  },
  loadModal(currentModal) {
    const modal = currentModal.cloneNode(true);
    currentModal.remove();
    const body = document.getElementsByTagName("body")[0];
    body.insertBefore(modal, body.firstChild);
    const listManager = this.list;
    MicroModal.init({
      onShow() {
        if (listManager.isBuilt) {
          return;
        }
        listManager.list.then((data) => {
          const options = {
            valueNames: ["name", "type"],
            item: "quick-list-data-item",
          };
          const list = new List("quick-list-data", options, data);
          const filter = modal.getElementsByClassName("js-type-filter")[0];
          filter.addEventListener("input", (e) => {
            const filterValue = e.target.value;
            if (filterValue === "any") {
              list.filter();
              return;
            }
            list.filter((item) => item.values().type === filterValue);
          });
          const search = modal.getElementsByClassName("js-search-input")[0];
          const buttonClear = modal.getElementsByClassName(
            "js-clear-filters"
          )[0];
          buttonClear.addEventListener("click", () => {
            filter.value = "any";
            search.value = "";
            list.filter();
            list.search("");
          });
          const liElements = modal.querySelectorAll("li.list-item");
          liElements.forEach((element) => {
            const name = element.getElementsByClassName("name")[0].innerText;
            const buttonFilter = element.getElementsByClassName(
              "attach button"
            )[0];
            buttonFilter.addEventListener("click", () => {
              const modalTrigger = listManager.triggeringField;
              document.querySelectorAll(
                `input.${modalTrigger}`
              )[0].value = name;
              MicroModal.close("quick-list-modal");
            });
          });
        });
      },
      awaitCloseAnimation: true,
    });
  },
  get modal() {
    return document.getElementById("quick-list-modal");
  },
  get list() {
    if (this._listManager === null) {
      this._listManager = new QuickList(
        Drupal.url("admin/config/media/library_field/ajax/simple_list")
      );
    }
    return this._listManager;
  },
  hasInit: false,
  _listManager: null,
};
