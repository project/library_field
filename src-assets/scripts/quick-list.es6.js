/**
 * Class to handle management of the list of libraries.
 */
export default class QuickList {
  /**
   * QuickList constructor.
   *
   * @constructor
   *
   * @param {string} endpoint - The library JSON endpoint.
   */
  constructor(endpoint) {
    this._quickListEndPoint = endpoint;
    this._init = false;
    this._triggeringField = null;
  }

  /**
   * Getter for the libraries data endpoint.
   *
   * @return {string} The endpoint
   */
  get quickListEndPoint() {
    return this._quickListEndPoint;
  }

  /**
   * Getter for library data.
   *
   * @return {Promise<any>} Promise of the json library data.
   */
  get data() {
    return fetch(this.quickListEndPoint).then((response) => response.json());
  }

  /**
   * If the quick list data has been called.
   *
   * @return {boolean} If the list data has been called.
   */
  get isBuilt() {
    return this._init;
  }

  /**
   * Gets the library data.
   *
   * @return {Promise<*>} Promise of the json library data.
   */
  get list() {
    this._init = true;
    return this.data;
  }

  /**
   * Set the trigger admin field for this list.
   *
   * @param {string|null} value - The field name
   */
  set triggeringField(value) {
    this._triggeringField = value;
  }

  /**
   * The admin field that triggered the quick modal view.
   *
   * @return {string|null} The triggering field
   */
  get triggeringField() {
    return this._triggeringField;
  }
}
